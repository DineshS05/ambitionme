package com.example.app1;

import android.os.Bundle;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ListofEmployee extends ListActivity {
	String[] s={"Narendar Wadhwa","Dinesh Sadhwani"};
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ArrayAdapter a1=new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, s);
		getListView().setChoiceMode(0);
		setListAdapter(a1);
		//setContentView(R.layout.activity_listof_employee);
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		if(position==0)
		{
		Intent i1=new Intent("com.example.app1.Narendar");
	startActivity(i1);
		}
	else
	{
		Intent i2=new Intent("com.example.app1.Darshan");
		startActivity(i2);
	}
	}
}
